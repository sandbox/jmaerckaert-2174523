<?php
/**
 * @file
 * Settings page for Countdown Poll.
 */

/**
 * Implements hook_form().
 */
function countdown_poll_settings_form() {
  $form['countdown_poll_jquery'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Jquery Countdown library'),
    '#description' => t('You need to install !url plugin by !url1 in sites/all/libraries', array('!url' => l(t('jQuery Countdown'), 'http://keith-wood.name/countdown.html'), '!url1' => l(t('Keith Wood'), 'http://keith-wood.name/countdown.html'))),
    '#default_value' => variable_get('countdown_poll_jquery'),
  );

  $form['countdown_poll_jquery_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use CSS styles from Jquery Countdown library'),
    '#description' => t('You need to install !url plugin by !url1 in sites/all/libraries', array('!url' => l(t('jQuery Countdown'), 'http://keith-wood.name/countdown.html'), '!url1' => l(t('Keith Wood'), 'http://keith-wood.name/countdown.html'))),
    '#default_value' => variable_get('countdown_poll_jquery_css'),
  );

  return system_settings_form($form);
}